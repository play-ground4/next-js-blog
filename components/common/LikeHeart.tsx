import { FC } from "react";
import { BiLoader } from "react-icons/bi";
import { BsHeart, BsHeartFill } from "react-icons/bs";

interface Props {
  busy?: boolean;
  label?: string;
  liked?: boolean;
  onClick?(): void;
}

const LikeHeart: FC<Props> = ({
  liked = false,
  label,
  busy = false,
  onClick,
}): JSX.Element => {
  return (
    <button
      type="button"
      className="text-primary-dark dark:text-primary flex items-center space-x-2 outline-none"
    >
      {  
      busy ? <BiLoader className="animate-spin"/> :     
      (liked ? <BsHeartFill color="#4790FD" onClick={onClick}/> : <BsHeart onClick={onClick}/>)
      }
      <span>{label}</span>
    </button>
  );
};

export default LikeHeart;
